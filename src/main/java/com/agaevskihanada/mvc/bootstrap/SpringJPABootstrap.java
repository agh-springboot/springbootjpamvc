package com.agaevskihanada.mvc.bootstrap;

import com.agaevskihanada.mvc.domain.*;
import com.agaevskihanada.mvc.domain.security.Role;
import com.agaevskihanada.mvc.enums.OrderStatus;
import com.agaevskihanada.mvc.services.ProductService;
import com.agaevskihanada.mvc.services.RoleService;
import com.agaevskihanada.mvc.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class SpringJPABootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private ProductService productService;
    private UserService userService;
    private RoleService roleService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        loadProducts();
        loadUsersAndCustomers();
        loadCarts();
        loadOrderHistory();
        loadRoles();
        assignUsersToDefaultRole();
    }

    private void assignUsersToDefaultRole(){
        List<Role> roles = (List<Role>) roleService.listAll();
        List<User> users = (List<User>) userService.listAll();

        roles.forEach(role -> {
            if (role.getRole().equalsIgnoreCase("CUSTOMER")){
                users.forEach(user -> {
                    user.addRole(role);
                    userService.saveOrUpdate(user);
                });
            }
        });
    }

    private void loadRoles(){
        Role role = new Role();
        role.setRole("CUSTOMER");
        roleService.saveOrUpdate(role);
    }

    private void loadOrderHistory() {
        List<User> users = (List<User>) userService.listAll();
        List<Product> products = (List<Product>) productService.listAll();

        users.forEach(user -> {
            Order order = new Order();
            order.setCustomer(user.getCustomer());
            order.setOrderStatus(OrderStatus.SHIPPED);

            products.forEach(product -> {
                OrderDetail orderDetail =new OrderDetail();
                orderDetail.setProduct(product);
                orderDetail.setQuantity(1);
                order.addToOrderDetails(orderDetail);
            });
        });

    }

    private void loadCarts() {
        List<User> users = (List<User>) userService.listAll();
        List<Product> products = (List<Product>) productService.listAll();

        users.forEach(user -> {
            user.setCart(new Cart());
            CartDetail cartDetail = new CartDetail();
            cartDetail.setProduct(products.get(0));
            cartDetail.setQuantity(2);
            user.getCart().addCartDetail(cartDetail);
            userService.saveOrUpdate(user);
        });
    }

    private void loadUsersAndCustomers() {

        User user1 = new User();
        user1.setUsername("mihk");
        user1.setPassword("typical_passs");

        Customer customer1 = new Customer();
        customer1.setFirstName("Mihail");
        customer1.setLastName("K");
        customer1.setBillingAddress(new Address());
        customer1.getBillingAddress().setAddressLine1("Pushkina 40");
        customer1.getBillingAddress().setCity("Minsk");
        customer1.getBillingAddress().setState("Belarus");
        customer1.getBillingAddress().setZipCode("33101");
        customer1.setEmail("mihailk@burnnotice.com");
        customer1.setPhoneNumber("375-29-342-12-56");

        user1.setCustomer(customer1);
        userService.saveOrUpdate(user1);


        User user2 = new User();
        user2.setUsername("fglen");
        user2.setPassword("simple_passs");

        Customer customer2 = new Customer();
        customer2.setFirstName("Fiona");
        customer2.setLastName("Glenanne");
        customer2.setBillingAddress(new Address());
        customer2.getBillingAddress().setAddressLine1("1 Key Biscane Ave");
        customer2.getBillingAddress().setCity("Miami");
        customer2.getBillingAddress().setState("Florida");
        customer2.getBillingAddress().setZipCode("33101");
        customer2.setEmail("fiona@example.com");
        customer2.setPhoneNumber("305.323.0233");

        user2.setCustomer(customer2);
        userService.saveOrUpdate(user2);

        User user3 = new User();
        user3.setUsername("mikop");
        user3.setPassword("lazy_passs");

        Customer customer3 = new Customer();
        customer3.setFirstName("Mikola");
        customer3.setLastName("P");
        customer3.setBillingAddress(new Address());
        customer3.getBillingAddress().setAddressLine1("vul. Schevchenko");
        customer3.getBillingAddress().setCity("Kiev");
        customer3.getBillingAddress().setState("Ukraine");
        customer3.getBillingAddress().setZipCode("33104");
        customer3.setEmail("mikolapp@protonmail.com");
        customer3.setPhoneNumber("+380-89-343-98-24");

        user3.setCustomer(customer3);
        userService.saveOrUpdate(user3);
    }

    public void loadProducts(){
        Product product1 = new Product();
        product1.setDescription("Product 1");
        product1.setPrice(new BigDecimal("12.99"));
        product1.setImageUrl("http://example.com/product1");
        productService.saveOrUpdate(product1);

        Product product2 = new Product();
        product2.setDescription("Product 2");
        product2.setPrice(new BigDecimal("14.99"));
        product2.setImageUrl("http://example.com/product2");
        productService.saveOrUpdate(product2);



        Product product3 = new Product();
        product3.setDescription("Product 3");
        product3.setPrice(new BigDecimal("34.99"));
        product3.setImageUrl("http://example.com/product3");
        productService.saveOrUpdate(product3);


        Product product4 = new Product();
        product4.setDescription("Product 4");
        product4.setPrice(new BigDecimal("43.99"));
        product4.setImageUrl("http://example.com/product4");
        productService.saveOrUpdate(product4);


        Product product5 = new Product();
        product5.setDescription("Product 5");
        product5.setPrice(new BigDecimal("56.99"));
        product5.setImageUrl("http://example.com/product5");
        productService.saveOrUpdate(product5);

    }
}
