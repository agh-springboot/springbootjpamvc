package com.agaevskihanada.mvc.enums;

public enum OrderStatus {
    NEW, ALLOCATED, SHIPPED
}
