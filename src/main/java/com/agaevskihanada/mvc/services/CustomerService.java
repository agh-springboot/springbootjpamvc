package com.agaevskihanada.mvc.services;

import com.agaevskihanada.mvc.domain.Customer;

public interface CustomerService extends CRUDService<Customer> {
}
