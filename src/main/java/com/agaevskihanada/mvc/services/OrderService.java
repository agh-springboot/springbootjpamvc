package com.agaevskihanada.mvc.services;

import com.agaevskihanada.mvc.domain.Order;

public interface OrderService extends CRUDService<Order>{
}
