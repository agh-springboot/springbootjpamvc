package com.agaevskihanada.mvc.services;

import com.agaevskihanada.mvc.domain.Product;

import java.util.List;

public interface ProductService extends CRUDService<Product> {

}
