package com.agaevskihanada.mvc.services;

import com.agaevskihanada.mvc.domain.security.Role;

public interface RoleService extends CRUDService<Role>{
}
