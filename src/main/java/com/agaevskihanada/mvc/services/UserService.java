package com.agaevskihanada.mvc.services;

import com.agaevskihanada.mvc.domain.User;

public interface UserService extends CRUDService<User> {
}
