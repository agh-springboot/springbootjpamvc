package com.agaevskihanada.mvc.services.jpaservices;

import com.agaevskihanada.mvc.domain.Customer;
import com.agaevskihanada.mvc.services.CustomerService;
import com.agaevskihanada.mvc.services.security.EncryptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Service
@Profile("jpadao")
public class CustomerServiceJpaDaoImpl extends AbstractJpaDaoService implements CustomerService {

    private EncryptionService encryptionService;

    @Autowired
    public void setEncryptionService(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    @Override
    public List<Customer> listAll() {
        EntityManager entityManager = emf.createEntityManager();
        List<Customer> list = entityManager.createQuery("from Customer", Customer.class).getResultList();
        entityManager.close();
        return list;
    }

    @Override
    public Customer getById(Integer id) {
        EntityManager entityManager = emf.createEntityManager();
        Customer customer = entityManager.find(Customer.class,id);
        entityManager.close();
        return customer;   }

    @Override
    public Customer saveOrUpdate(Customer domainObject) {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();

        if (domainObject.getUser()!=null && domainObject.getUser().getPassword()!=null){
            domainObject.getUser().setEncryptedPassword(
                    encryptionService.encryptString(domainObject.getUser().getPassword()));
        }

        Customer savedCustomer = entityManager.merge(domainObject);
        entityManager.getTransaction().commit();
        entityManager.close();
        return savedCustomer;    }

    @Override
    public void delete(Integer id) {
        EntityManager entityManager = emf.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.remove(entityManager.find(Customer.class,id ));
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
