package com.agaevskihanada.mvc.services.jpaservices;

import com.agaevskihanada.mvc.domain.Order;
import com.agaevskihanada.mvc.services.OrderService;
import org.aspectj.weaver.ast.Or;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Profile("jpadao")
public class OrderServiceJpaDaoImpl extends AbstractJpaDaoService implements OrderService {
    @Override
    public List<Order> listAll() {
        EntityManager entityManager = emf.createEntityManager();

        List<Order> list = entityManager.createQuery("from Order", Order.class).getResultList();
        entityManager.close();
        return list;
    }

    @Override
    public Order getById(Integer id) {
        EntityManager entityManager = emf.createEntityManager();
        Order order = entityManager.find(Order.class,id );
        entityManager.close();
        return order;
    }

    @Override
    public Order saveOrUpdate(Order domainObject) {
        EntityManager entityManager = emf.createEntityManager();

        entityManager.getTransaction().begin();
        Order savedProduct = entityManager.merge(domainObject);
        entityManager.getTransaction().commit();
        entityManager.close();
        return savedProduct;
    }

    @Override
    public void delete(Integer id) {
        EntityManager entityManager = emf.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.remove(entityManager.find(Order.class, id));
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
