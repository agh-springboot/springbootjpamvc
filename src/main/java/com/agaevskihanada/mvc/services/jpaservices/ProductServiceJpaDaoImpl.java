package com.agaevskihanada.mvc.services.jpaservices;

import com.agaevskihanada.mvc.domain.Product;
import com.agaevskihanada.mvc.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Service
@Profile("jpadao")
public class ProductServiceJpaDaoImpl extends AbstractJpaDaoService implements ProductService {

    @Override
    public List<Product> listAll() {
        EntityManager entityManager = emf.createEntityManager();

        List<Product> list = entityManager.createQuery("from Product", Product.class).getResultList();
        entityManager.close();
        return list;
    }

    @Override
    public Product getById(Integer id) {
        EntityManager entityManager = emf.createEntityManager();
        Product product = entityManager.find(Product.class,id);
        entityManager.close();
        return product;
    }

    @Override
    public Product saveOrUpdate(Product domainObject) {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        Product savedProduct = entityManager.merge(domainObject);
        entityManager.getTransaction().commit();
        entityManager.close();
        return savedProduct;
    }

    @Override
    public void delete(Integer id) {
        EntityManager entityManager = emf.createEntityManager();

        entityManager.getTransaction().begin();
        entityManager.remove(entityManager.find(Product.class,id ));
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
