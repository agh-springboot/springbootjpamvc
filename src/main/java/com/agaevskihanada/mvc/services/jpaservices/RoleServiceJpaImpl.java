package com.agaevskihanada.mvc.services.jpaservices;

import com.agaevskihanada.mvc.domain.security.Role;
import com.agaevskihanada.mvc.services.RoleService;
import com.agaevskihanada.mvc.services.mapservices.AbstractMapService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Profile("jpadao")
public class RoleServiceJpaImpl extends AbstractJpaDaoService implements RoleService {
    @Override
    public List<?> listAll() {
        EntityManager em = emf.createEntityManager();
        List<Role> list = em.createQuery("from Role",Role.class).getResultList();
        em.close();
        return list;
    }

    @Override
    public Role getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        Role role = em.find(Role.class,id );
        em.close();
        return role;
    }

    @Override
    public Role saveOrUpdate(Role domainObject) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        Role savedRole = em.merge(domainObject);
        em.getTransaction().commit();
        em.close();

        return savedRole;
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        em.remove(em.find(Role.class,id ));
        em.getTransaction().commit();

        em.close();
    }
}
