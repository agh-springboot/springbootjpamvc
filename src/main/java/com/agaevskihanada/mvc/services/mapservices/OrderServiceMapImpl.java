package com.agaevskihanada.mvc.services.mapservices;

import com.agaevskihanada.mvc.domain.DomainObject;
import com.agaevskihanada.mvc.domain.Order;
import com.agaevskihanada.mvc.services.OrderService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("map")
public class OrderServiceMapImpl extends AbstractMapService implements OrderService {
    @Override
    public Order saveOrUpdate(Order domainObject) {
        return (Order) super.saveOrUpdate(domainObject);
    }

    @Override
    public List<DomainObject> listAll(){
        return super.listAll();
    }

    @Override
    public void delete(Integer id){
        super.delete(id);
    }

    @Override
    public Order getById(Integer id) {
        return (Order)super.getById(id);
    }
}
