package com.agaevskihanada.mvc.services.security;

public interface EncryptionService {
    String encryptString(String input);
    boolean CheckPassword(String plainPassword, String encryptedPassword);
}
