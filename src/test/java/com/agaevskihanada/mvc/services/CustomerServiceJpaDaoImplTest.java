package com.agaevskihanada.mvc.services;

import com.agaevskihanada.mvc.domain.Customer;
import com.agaevskihanada.mvc.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("jpadao")
public class CustomerServiceJpaDaoImplTest {

    private CustomerService customerService;

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Test
    public void testList() throws Exception{
        List<Customer> products = (List) customerService.listAll();

        assert products.size() == 3;
    }

    @Test
    public void testSaveWithUser(){
        Customer customer = new Customer();
        User user = new User();
        user.setUsername("UserNameTest");
        user.setPassword("MyPasss");
        customer.setUser(user);

        Customer savedCustomer =  customerService.saveOrUpdate(customer);
        assert  savedCustomer.getUser().getId()!=null;
    }
}
